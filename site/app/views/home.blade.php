<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Sombrero simulation homepage</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/css/normalize.css">
        <link rel="stylesheet" href="/css/main.css">
    </head>
    <body>
        <header class="header">
            <h1>Sombrero simulation homepage</h1>
        </header>
    </body>
</html>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Page {{$page}}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="/css/normalize.css">
        <link rel="stylesheet" href="/css/main.css">
        <?php if(isset($page_data['video'])) { ?>
        <link href="http://vjs.zencdn.net/4.2/video-js.css" rel="stylesheet">
        <script src="http://vjs.zencdn.net/4.2/video.js"></script>
        <?php } ?>
        <style type="text/css">
            body {
                background-image: url(/assets/pages/{{$page}}.{{$page_data['image_ext']}});
                background-position: center top;
                background-repeat: no-repeat;
                height: {{$page_data['image_size'][1]+150}}px;
            }
            <?php if(isset($page_data['video'])) { ?>
            video {
                position: absolute;
                top: {{$page_data['video']['position_top']}}px;
                left: {{$page_data['video']['position_left']}}px;
            }
            <?php } ?>
        </style>
    </head>
    <body>
    <?php if(isset($page_data['video'])) { ?>
        <video id="{{$page}}" class="video-js vjs-default-skin" controls preload="auto" width="{{$page_data['video']['width']}}" height="{{$page_data['video']['height']}}">
            <source src="{{$page_data['video']['src']}}" type='video/mp4'>
        </video>

    <?php } ?>
        <footer class="footer">
            <h1>Scenario only – does not represent actual events – for training purposes only</h1>
            <p>If you experience any technical difficulties with the simulation, please email <a href="mailto:sombrero.nvd@novartis.com">sombrero.nvd@novartis.com</a></p>
        </footer>
    </body>
</html>

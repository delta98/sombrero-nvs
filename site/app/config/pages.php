<?php

return array(
	'17D01' => array(
		'description' => 'Test description',
        'image_ext'   => 'jpg'
	),
    '38D10' => array(
        'description' => 'Youtube Simulation Page',
        'image_ext' => 'png',
        'video' => array(
            'src'           => 'http://sombrero-nvs.com/assets/videos/YouTube.mp4', // @TODO Change link to actual vimeo player url
            'width'         => '640', // iframe width
            'height'        => '390', // iframe height
            'position_top'  => '60', // iframe position top
            'position_left' => '198', // iframe position left
        )
    ),
    '20D03' => array(
        'description' => 'BBC Simulation Page',
        'image_ext' => 'jpg',
        'video' => array(
            'src'           => 'http://sombrero-nvs.com/assets/videos/BBCNewsHD.mp4', // @TODO Change link to actual vimeo player url
            'width'         => '479', // iframe width
            'height'        => '270', // iframe height
            'position_top'  => '166', // iframe position top
            'position_left' => '142', // iframe position left
        )
    ),
    '32D06' => array(
        'description' => 'SKY NEWS Simulation Page',
        'image_ext' => 'jpg',
        'video' => array(
            'src'           => 'http://sombrero-nvs.com/assets/videos/SkyNewsHD.mp4', // @TODO Change link to actual vimeo player url
            'width'         => '523', // iframe width
            'height'        => '301', // iframe height
            'position_top'  => '489', // iframe position top
            'position_left' => '167', // iframe position left
        )
    )
);

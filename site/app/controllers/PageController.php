<?php

class PageController extends BaseController {

	public function showPage($page)
	{
		if ( ! $page_data = Config::get('pages.'.$page))
		{
			App::abort(404, 'Page not found');
		}

		$page_data['image_size'] = getimagesize(public_path().'/assets/pages/'.$page.'.'.$page_data['image_ext']);

		return View::make('page', array('page' => $page, 'page_data' => $page_data));
	}

	public function showHome()
	{
		return View::make('home');
	}
}
